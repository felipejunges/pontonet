using FluentValidation;
using MediatR;
using PontoNet.Api.Middlewares;
using PontoNet.Domain;
using PontoNet.Domain.Behaviors;
using PontoNet.Domain.Interfaces;
using PontoNet.Domain.Interfaces.Repositories;
using PontoNet.Domain.Notifications;
using PontoNet.Domain.Notifications.Interfaces;
using PontoNet.Infra.Context;
using PontoNet.Infra.Repositories;
using System.Text.Json.Serialization;

var myAllowSpecificOrigins = "_myAllowSpecificOrigins";

var builder = WebApplication.CreateBuilder(args);

// Begin of IOT
builder.Services.AddScoped(typeof(IPipelineBehavior<,>), typeof(ValidationPipelineBehavior<,>));
builder.Services.AddValidatorsFromAssembly(AssemblyReference.Assembly, includeInternalTypes: true);

builder.Services.AddMediatR(cfg =>
{
    cfg.RegisterServicesFromAssembly(AssemblyReference.Assembly);
    cfg.AddBehavior(typeof(IPipelineBehavior<,>), typeof(ValidationPipelineBehavior<,>));
});

builder.Services.AddScoped<INotificationContext, NotificationContext>();

builder.Services.AddScoped<IFechamentoMesRepository, FechamentoMesRepository>();
builder.Services.AddScoped<IRegistroRepository, RegistroRepository>();
builder.Services.AddScoped<IUnitOfWork, UnitOfWork>();

// isso faz o projeto Api ser dependente do EFCore... (Microsoft.EntityFrameworkCore)
builder.Services.AddDbContext<PontoContext>();
// End of IOT

builder.Services
    .AddControllers()
    //.AddControllers(options => options.Filters.Add(typeof(NotificationFilter)))
    .AddJsonOptions(options => options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter()));

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddCors(options =>
{
    options.AddPolicy(myAllowSpecificOrigins,
                      policy =>
                      {
                          policy.WithOrigins("http://localhost:5220")
                                .AllowAnyHeader()
                                .AllowAnyMethod();
                      });
});

var app = builder.Build();

app.UseMiddleware(typeof(ErrorHandlingMiddleware));

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseCors(myAllowSpecificOrigins);

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
