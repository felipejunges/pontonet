﻿using System.Text.RegularExpressions;

namespace PontoNet.Domain.ValueObjects
{
    public sealed partial record Hora
    {
        public string? Value { get; }

        public TimeSpan TimeValue =>
            !IsValid()
                ? TimeSpan.Zero
                : new TimeSpan(int.Parse(Value![0..2]), int.Parse(Value[3..5]), 0);

        public Hora(string? hora)
        {
            Value = hora;
        }

        public bool IsValid()
        {
            return Value is not null && Regex.IsMatch(Value, "^[01][0-9]|2[0-3]:[0-5][0-9]$");
        }

        public static implicit operator Hora(string? hora) => new Hora(hora);
    }
}