using PontoNet.Domain.Validators;

namespace PontoNet.Domain.Entities
{
    public class Registro// : Entity
    {
        public long Id { get; private set; }

        public DateTime Data { get; private set; }

        public TimeSpan HoraInicial { get; private set; }

        public TimeSpan? HoraFinal { get; private set; }

        public double Horas => !HoraFinal.HasValue
                                    ? (DateTime.Now.TimeOfDay - HoraInicial).TotalHours
                                    : (HoraFinal.Value - HoraInicial).TotalHours + ((HoraFinal.Value < HoraInicial) ? 24D : 0D);

        public Registro(DateTime data, TimeSpan horaInicial)
        {
            this.Data = data;
            this.HoraInicial = horaInicial;

            //Validate(this, new RegistroValidator());
        }

        public Registro(DateTime data, TimeSpan horaInicial, TimeSpan horaFinal)
            : this(data, horaInicial)
        {
            this.HoraFinal = horaFinal;

            //Validate(this, new RegistroValidator());
        }

        public void AlterarDados(DateTime data, TimeSpan horaInicial, TimeSpan? horaFinal)
        {
            Data = data;
            HoraInicial = horaInicial;
            HoraFinal = horaFinal;
        }

        public void AlterarHoraFinal(TimeSpan horaFinal)
        {
            HoraFinal = horaFinal;
        }
    }
}