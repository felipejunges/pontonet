using MediatR;
using PontoNet.Domain.ValueObjects;

namespace PontoNet.Domain.Commands.Registros.ObterResumoDiario
{
    public sealed record ObterResumoDiarioCommand : IRequest<ResumoData>;
}