using MediatR;

namespace PontoNet.Domain.Commands.Registros.ExcluirRegistro
{
    public sealed record ExcluirRegistroCommand(long Id) : IRequest;
}