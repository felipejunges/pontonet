using MediatR;
using PontoNet.Domain.Entities;

namespace PontoNet.Domain.Commands.Registros.RegistrarHorarioData
{
    public sealed record RegistrarHorarioDataCommand(DateTime Data) : IRequest<Registro>;
}