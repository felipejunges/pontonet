using MediatR;

namespace PontoNet.Domain.Commands.Registros.AlterarRegistro
{
    public sealed record AlterarRegistroCommand(long Id, DateTime Data, string HoraInicial, string HoraFinal) : IRequest;
}