using MediatR;
using PontoNet.Domain.Entities;

namespace PontoNet.Domain.Commands.Registros.ListarRegistrosData
{
    public sealed record ListarRegistrosDataCommand(DateTime Data) : IRequest<IEnumerable<Registro>>;
}