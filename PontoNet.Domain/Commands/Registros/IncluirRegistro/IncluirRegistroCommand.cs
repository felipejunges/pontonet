using MediatR;
using PontoNet.Domain.Entities;
using PontoNet.Domain.ValueObjects;

namespace PontoNet.Domain.Commands.Registros.IncluirRegistro
{
    public sealed record IncluirRegistroCommand(DateTime? Data, Hora? Hora) : IRequest<Registro?>;
}