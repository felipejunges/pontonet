using FluentValidation;

namespace PontoNet.Domain.Commands.Registros.IncluirRegistro
{
    public class IncluirRegistroCommandValidator : AbstractValidator<IncluirRegistroCommand>
    {
        public IncluirRegistroCommandValidator()
        {
            RuleFor(r => r.Data).NotEmpty().WithMessage("A data � obrigat�ria");

            RuleFor(r => r.Hora).NotEmpty().WithMessage("A hora � obrigat�ria");

            RuleFor(r => r.Hora).Must(r => r!.IsValid()).When(r => r.Hora is not null).WithMessage("A hora deve ser informada corretamente");
        }
    }
}