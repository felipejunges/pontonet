using MediatR;
using PontoNet.Domain.Entities;
using PontoNet.Domain.Interfaces;
using PontoNet.Domain.Interfaces.Repositories;
using PontoNet.Domain.Notifications.Interfaces;

namespace PontoNet.Domain.Commands.Registros.IncluirRegistro
{
    public class IncluirRegistroCommandHandler : IRequestHandler<IncluirRegistroCommand, Registro?>
    {
        private readonly IRegistroRepository _registroRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly INotificationContext _notificationContext;

        public IncluirRegistroCommandHandler(IRegistroRepository registroRepository, IUnitOfWork unitOfWork, INotificationContext notificationContext)
        {
            _registroRepository = registroRepository;
            _unitOfWork = unitOfWork;
            _notificationContext = notificationContext;
        }

        public async Task<Registro?> Handle(IncluirRegistroCommand request, CancellationToken cancellationToken)
        {
            //_notificationContext.AddNotification("Algo aconteceu");
            //return default;

            var registro = new Registro(
                request.Data!.Value,
                request.Hora!.TimeValue
            );

            await _registroRepository.IncluirRegistroAsync(registro);

            await _unitOfWork.SaveChangesAsync();

            return registro;
        }
    }
}